/*******************************************************************************
 * Copyright 2014 Davide Barbieri, Emanuele Della Valle, Marco Balduini
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Acknowledgements:
 * 
 * This work was partially supported by the European project LarKC (FP7-215535) 
 * and by the European project MODAClouds (FP7-318484)
 ******************************************************************************/
package ObservationRDFStreamer;

import java.util.Random;

import eu.larkc.csparql.cep.api.RdfQuadruple;
import eu.larkc.csparql.cep.api.RdfStream;

public class DiseaseStreamer extends RdfStream implements Runnable  {
	
	private String baseUri;
	private long sleepTime;
	private Random random;
	
	public static String ssnNamespace = "http://www.w3.org/ns/ssn/"; 
	public static String sosaNamespace = "http://www.w3.org/ns/sosa/";
	private int obsNum = 0;

	public DiseaseStreamer(String iri, String baseUri,long sleepTime) {
		super(iri);
		this.sleepTime = sleepTime;
		this.baseUri = baseUri;
		random = new Random();
	}
	public void run() {
		
		Random random = new Random();
		
		String disease;
		long dateTime;
		int apartement;
		int observationIndex;

		while(true){
			try{
				
				disease = "anything";
				apartement = random.nextInt(5);
				observationIndex = random.nextInt(5);
				//roomIndex = random.nextInt(5);
				dateTime = 1234567;

				RdfQuadruple q = new RdfQuadruple(baseUri + "observation" + observationIndex, baseUri + "observes", 
						baseUri + "disease"+ disease, dateTime);

				q = new RdfQuadruple(baseUri + "disease" + disease, baseUri + "detectedIn", 
						baseUri + "apartment" + apartement, dateTime);
				this.put(q);				
	
				q = new RdfQuadruple(baseUri + "disease" + disease, baseUri + "detectedAt", 
						baseUri+"dateTime" + dateTime, dateTime);
					this.put(q);
				
				Thread.sleep(sleepTime);				
			} catch(Exception e){
				e.printStackTrace();
			}
		}

	}
	
	public void addToStream (String disease, int apart, String day, long dateTime){
		
		
		//int observationIndex = random.nextInt(100);
		
		RdfQuadruple q = new RdfQuadruple(ssnNamespace+"DiseaseObservation"+ obsNum, 
				sosaNamespace + "observedProperty", 
				ssnNamespace+ "disease", 
				dateTime);
				//System.currentTimeMillis());
		this.put(q);

		q = new RdfQuadruple(ssnNamespace+"DiseaseObservation"+ obsNum, 
				sosaNamespace + "hasFeatureOfInterest", 
				ssnNamespace + "apartment" +apart, 
				dateTime);
				//System.currentTimeMillis());
		this.put(q);
		
		q = new RdfQuadruple(ssnNamespace+"DiseaseObservation"+ obsNum, 
				sosaNamespace + "hasSimpleResult", 
				String.valueOf(disease)+ "^^http://www.w3.org/2001/XMLSchema#string",
				dateTime);
				//System.currentTimeMillis());
		this.put(q);

		/*q = new RdfQuadruple(ssnNamespace+"SymptomObservation"+ observationIndex, 
				sosaNamespace + "hasSimpleResult", 
				String.valueOf(value) + "^^http://www.w3.org/2001/XMLSchema#integer",
				dateTime);
				//System.currentTimeMillis());
		this.put(q);*/
		
		q = new RdfQuadruple(ssnNamespace+"DiseaseObservation"+ obsNum, 
				sosaNamespace + "resultTime", 
				String.valueOf(day) + "^^http://www.w3.org/2001/XMLSchema#string", 
				dateTime);
				//System.currentTimeMillis());
		this.put(q);
		obsNum++;
	}

}
