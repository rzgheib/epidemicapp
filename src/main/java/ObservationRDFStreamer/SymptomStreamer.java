/*******************************************************************************
 * Copyright 2014 Davide Barbieri, Emanuele Della Valle, Marco Balduini
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Acknowledgements:
 * 
 * This work was partially supported by the European project LarKC (FP7-215535) 
 * and by the European project MODAClouds (FP7-318484)
 ******************************************************************************/
package ObservationRDFStreamer;

import java.util.ArrayList;
import java.util.Random;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.impl.PropertyImpl;
import com.hp.hpl.jena.rdf.model.impl.ResourceImpl;

import eu.larkc.csparql.cep.api.RdfQuadruple;
import eu.larkc.csparql.cep.api.RdfStream;

public class SymptomStreamer extends RdfStream implements Runnable  {
	
	private String baseUri;
	private long sleepTime;
	private Random random;
	private Model m;
	
	public static String ssnNamespace = "http://www.w3.org/ns/ssn/"; 
	public static String sosaNamespace = "http://www.w3.org/ns/sosa/";


	public SymptomStreamer(String iri, String baseUri,long sleepTime) {
		super(iri);
		this.sleepTime = sleepTime;
		this.baseUri = baseUri;
		random = new Random();
	}
	public void run() {
		
		Random random = new Random();
		
		String symptom;
		long dateTime;
		int apartement;
		int observationIndex;

		while(true){
			try{
				
				symptom = "anything";
				apartement = random.nextInt(5);
				observationIndex = random.nextInt(5);
				//roomIndex = random.nextInt(5);
				dateTime = 1234567;

				RdfQuadruple q = new RdfQuadruple(baseUri + "observation" + observationIndex, baseUri + "observedProperty", 
						baseUri + "symptom"+ symptom, dateTime);

				q = new RdfQuadruple(baseUri + "disease" + symptom, baseUri + "detectedIn", 
						baseUri + "apartment" + apartement, dateTime);
				this.put(q);				
	
				q = new RdfQuadruple(baseUri + "disease" + symptom, baseUri + "detectedAt", 
						baseUri+"dateTime" + dateTime, dateTime);
					this.put(q);
				
				Thread.sleep(sleepTime);				
			} catch(Exception e){
				e.printStackTrace();
			}
		}

	}
	
	public void addToStream (String property, int apart, long dateTime, int value){		
		
		int observationIndex = random.nextInt(5);
		//m = ModelFactory.createDefaultModel();
		
		/*ResourceImpl obs = new ResourceImpl(ssnNamespace+"ADLSimObservation"+ observationIndex);
		PropertyImpl hasSimpleResult = new PropertyImpl(sosaNamespace + "hasSimpleResult");
		PropertyImpl resultTime = new PropertyImpl(sosaNamespace + "resultTime");*/
		
		try {
		/*	m.add(obs, 
					new PropertyImpl(sosaNamespace + "observedProperty"), 
					new ResourceImpl(ssnNamespace+ property));
			
			m.add(obs, 
					new PropertyImpl(sosaNamespace + "hasFeatureOfInterest"), 
					new ResourceImpl(ssnNamespace+ apart));
			
			m.add(obs, 
					new PropertyImpl(sosaNamespace + "resultTime"), 
					new ResourceImpl(ssnNamespace+ dateTime));
			
			m.addLiteral (obs, resultTime, dateTime);
			m.addLiteral (obs, hasSimpleResult, value);*/
			
			
			RdfQuadruple q = new RdfQuadruple(ssnNamespace+"ADLSimObservation"+ observationIndex, 
					sosaNamespace + "observedProperty", 
					ssnNamespace+ property, 
					dateTime);
					//System.currentTimeMillis());
			this.put(q);

			q = new RdfQuadruple(ssnNamespace+"ADLSimObservation"+ observationIndex, 
					sosaNamespace + "hasFeatureOfInterest", 
					ssnNamespace + "apartment" +apart, 
					dateTime);
					//System.currentTimeMillis());
			this.put(q);

			q = new RdfQuadruple(ssnNamespace+"ADLSimObservation"+ observationIndex, 
					sosaNamespace + "hasSimpleResult", 
					String.valueOf(value) + "^^http://www.w3.org/2001/XMLSchema#integer",
					dateTime);
					//System.currentTimeMillis());
			this.put(q);
			
			q = new RdfQuadruple(ssnNamespace+"ADLSimObservation"+ observationIndex, 
					sosaNamespace + "resultTime", 
					String.valueOf(dateTime) + "^^http://www.w3.org/2001/XMLSchema#long", 
					dateTime);
					//System.currentTimeMillis());
			this.put(q);
		
			//System.out.println(q);
			//m.write(System.out);
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
