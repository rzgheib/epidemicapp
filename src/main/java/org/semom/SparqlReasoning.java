package org.semom;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import com.semom.adlsim.Epidemic.EpidemicApp;

public class SparqlReasoning {
	
	private ValueFactory vf;
	private Model model;
	private java.util.Date date;

	private Repository rep;
	private MemoryStore mem;
	private RDFFormat format;
	
	//File intput = new File("doid.owl");
	File intput = new File("doidGastro.owl");
	String prefix = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	String obo = "http://purl.obolibrary.org/obo/";
	String ontoIRI = "http://purl.obolibrary.org/obo/doid#";
	
	private String epidemicQuery;
	
	public SparqlReasoning() throws RDFParseException, RepositoryException, IOException{

	}
		
	public void initialize () throws RDFParseException, RepositoryException, IOException{		
		mem = new MemoryStore();
		mem.setPersist(false);
		//mem.initialize();
		rep = new SailRepository(mem);
		rep.initialize();
		vf = rep.getValueFactory();
		RDFParser rdfParser = Rio.createParser(RDFFormat.RDFXML);
		model = new LinkedHashModel();
		rdfParser.setRDFHandler(new StatementCollector(model));
		format = Rio.getParserFormatForFileName(intput.toString()).orElse(RDFFormat.RDFXML);
			
		System.out.println("Ontology initialized");
	}
	
	// this method is able to find the epidemic if 1 of the 4 symptoms exists.
	public String defineQuery (List<String> symptoms){ 
	//public String defineQuery (String symptom1, String symptom2, String symptom3, String symptom4){
		if (symptoms.size()==4)
			epidemicQuery = "prefix oio: <http://www.geneontology.org/formats/oboInOwl#>"
							+ "prefix owl: <http://www.w3.org/2002/07/owl#>"
							+ "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
							+ "prefix doi: <http://purl.obolibrary.org/obo/doid#>"
							+ "prefix DOID: <http://purl.obolibrary.org/obo/DOID_>"
							+ "prefix SYMP: <http://purl.obolibrary.org/obo/SYMP_>" 
							+ "SELECT ?baseLabel "
							+ "WHERE {"
							+ "?baseClass rdfs:subClassOf* ?superClass ."
							+ "?baseClass rdfs:label ?baseLabel ."
							
							+ "?baseClass rdfs:subClassOf ?symp1 ."
							+ "?symp1 a owl:Restriction ."
							+ "?symp1 owl:onProperty doi:has_symptom ."
							+ "?symp1 owl:someValuesFrom " + symptoms.get(0) + " ." //vomiting symptom
							
							+ "?baseClass rdfs:subClassOf ?symp2 ."
							+ "?symp2 a owl:Restriction ."
							+ "?symp2 owl:onProperty doi:has_symptom ."
							+ "?symp2 owl:someValuesFrom " + symptoms.get(1) + " ." // diarrhea
							
							+ "?baseClass rdfs:subClassOf ?symp3 ."
							+ "?symp3 a owl:Restriction ."
							+ "?symp3 owl:onProperty doi:has_symptom ."
							+ "?symp3 owl:someValuesFrom " + symptoms.get(2) + " ." // loss of appetite
							
							+ "?baseClass rdfs:subClassOf ?symp4 ."
							+ "?symp4 a owl:Restriction ."
							+ "?symp4 owl:onProperty doi:has_symptom ."
							+ "?symp4 owl:someValuesFrom " + symptoms.get(3) + " ." // nausea
							
							//+ "?baseClass doi:has_symptom ?symp ."
							+ "FILTER (?superClass = DOID:4)" // subclasses of disease
							+ "}";
		
		if (symptoms.size()==3)
			epidemicQuery = "prefix oio: <http://www.geneontology.org/formats/oboInOwl#>"
							+ "prefix owl: <http://www.w3.org/2002/07/owl#>"
							+ "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
							+ "prefix doi: <http://purl.obolibrary.org/obo/doid#>"
							+ "prefix DOID: <http://purl.obolibrary.org/obo/DOID_>"
							+ "prefix SYMP: <http://purl.obolibrary.org/obo/SYMP_>" 
							+ "SELECT ?baseLabel "
							+ "WHERE {"
							+ "?baseClass rdfs:subClassOf* ?superClass ."
							+ "?baseClass rdfs:label ?baseLabel ."
							
							+ "?baseClass rdfs:subClassOf ?symp1 ."
							+ "?symp1 a owl:Restriction ."
							+ "?symp1 owl:onProperty doi:has_symptom ."
							+ "?symp1 owl:someValuesFrom " + symptoms.get(0) + " ." 
							
							+ "?baseClass rdfs:subClassOf ?symp2 ."
							+ "?symp2 a owl:Restriction ."
							+ "?symp2 owl:onProperty doi:has_symptom ."
							+ "?symp2 owl:someValuesFrom " + symptoms.get(1) + " ." 
							
							+ "?baseClass rdfs:subClassOf ?symp3 ."
							+ "?symp3 a owl:Restriction ."
							+ "?symp3 owl:onProperty doi:has_symptom ."
							+ "?symp3 owl:someValuesFrom " + symptoms.get(2) + " ." 
							
							//+ "?baseClass doi:has_symptom ?symp ."
							+ "FILTER (?superClass = DOID:4)" // subclasses of disease
							+ "}";
		
		if (symptoms.size()==2)
			epidemicQuery = "prefix oio: <http://www.geneontology.org/formats/oboInOwl#>"
							+ "prefix owl: <http://www.w3.org/2002/07/owl#>"
							+ "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
							+ "prefix doi: <http://purl.obolibrary.org/obo/doid#>"
							+ "prefix DOID: <http://purl.obolibrary.org/obo/DOID_>"
							+ "prefix SYMP: <http://purl.obolibrary.org/obo/SYMP_>" 
							+ "SELECT ?baseLabel "
							+ "WHERE {"
							+ "?baseClass rdfs:subClassOf* ?superClass ."
							+ "?baseClass rdfs:label ?baseLabel ."
							
							+ "?baseClass rdfs:subClassOf ?symp1 ."
							+ "?symp1 a owl:Restriction ."
							+ "?symp1 owl:onProperty doi:has_symptom ."
							+ "?symp1 owl:someValuesFrom " + symptoms.get(0) + " ." //vomiting symptom
							
							+ "?baseClass rdfs:subClassOf ?symp2 ."
							+ "?symp2 a owl:Restriction ."
							+ "?symp2 owl:onProperty doi:has_symptom ."
							+ "?symp2 owl:someValuesFrom " + symptoms.get(1) + " ." // diarrhea
							
							//+ "?baseClass doi:has_symptom ?symp ."
							+ "FILTER (?superClass = DOID:4)" // subclasses of disease
							+ "}";
		
		if (symptoms.size()==1)
			epidemicQuery = "prefix oio: <http://www.geneontology.org/formats/oboInOwl#>"
							+ "prefix owl: <http://www.w3.org/2002/07/owl#>"
							+ "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
							+ "prefix doi: <http://purl.obolibrary.org/obo/doid#>"
							+ "prefix DOID: <http://purl.obolibrary.org/obo/DOID_>"
							+ "prefix SYMP: <http://purl.obolibrary.org/obo/SYMP_>" 
							+ "SELECT ?baseLabel "
							+ "WHERE {"
							+ "?baseClass rdfs:subClassOf* ?superClass ."
							+ "?baseClass rdfs:label ?baseLabel ."
							
							+ "?baseClass rdfs:subClassOf ?symp1 ."
							+ "?symp1 a owl:Restriction ."
							+ "?symp1 owl:onProperty doi:has_symptom ."
							+ "?symp1 owl:someValuesFrom " + symptoms.get(0) + " ." //vomiting symptom
							
							//+ "?baseClass doi:has_symptom ?symp ."
							+ "FILTER (?superClass = DOID:4)" // subclasses of disease
							+ "}";
		
		return epidemicQuery;
	}
		
	public BindingSet runQuery(){

		BindingSet bindingSet = null;
		
		try(RepositoryConnection con = rep.getConnection()) {
			
			   con.add(intput, ontoIRI, RDFFormat.RDFXML);
			   System.out.println("connection established. processing query...");
			   
		   try { 									                 		 
			   TupleQuery tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, epidemicQuery );
			   TupleQueryResult result = tupleQuery.evaluate();
			   try{
				   while (result.hasNext()) {  // iterate over the result
				   bindingSet = result.next();				   
				   //Value valueOfX = bindingSet.getValue("sensorOutput");
				   //System.out.println(bindingSet.toString());
	    		   //System.out.println();
				   }
				   System.out.println("query done succesffully");
				   }
			   finally {
			         result.close();
			         mem = new MemoryStore();
			      }
			}
			   finally {
			      con.close();
			   }
			}catch (RDF4JException e) {
			   // handle exception
				
				System.out.println("rdf exeption: " + e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return bindingSet;
	}
	/*	
	 public static void main( String[] args ) throws RDFParseException, RepositoryException, IOException {
		 SparqlReasoning query = new SparqlReasoning();
		 ArrayList<String> symptomsListt = new ArrayList<String>();
		symptomsListt.add("SYMP:0019145");
		//symptomsListt.add("SYMP:0000570");
		symptomsListt.add("SYMP:0000309");
		//symptomsListt.add("SYMP:0000458");
		 
		 query.defineQuery(symptomsListt);
		// query.defineQuery();
	     query.runQuery();
	    }
*/
}
