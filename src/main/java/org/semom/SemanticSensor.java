package org.semom;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.apache.commons.lang3.tuple.Triple;
import org.apache.http.client.utils.URIBuilder;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.util.RDFCollections;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.binary.BinaryRDFParserFactory;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.memory.MemoryStore;


//this class is generic and all other semantic sensors inherits from this one
public class SemanticSensor {

	private String type;
	private String name;
	private String property;
	private String foi;
	
	public static File file = new File("CoSSN_V1_epidemic.owl");	
	//public static String baseURI = "http://www.semanticweb.org/rzgheib/ontologies/2016/11/CoSSN#";
	//public static String baseURI = "http://www.w3.org/ns/ssn/";
	//public static String cossnNamespace = "http://www.semanticweb.org/rzgheib/ontologies/2016/11/CoSSN#";
	public static String ssnNamespace = "http://www.w3.org/ns/ssn/"; 
	public static String sosaNamespace = "http://www.w3.org/ns/sosa/";
	
	private Model model;
	private Model sensorOutputModel;
	private java.util.Date date;
	private ValueFactory f;
	private Repository rep;
	private MemoryStore mem;
	private RDFFormat format;
	
	RepositoryConnection conn;
	
	private int obsNum = 0;
	
	// classes IRIR
	private IRI Sensor;
	private IRI Property;
	private IRI FeatureOfInterest;
	private IRI ApartmentID;
	private IRI Observation;
	private IRI Result;
		
	// Object properties IRI
	private IRI observes;
	private IRI hasProperty;
	private IRI hasDeployment;
	private IRI madeBySensor;
	private IRI observedProperty;
	private IRI hasResult;
	private IRI hasFeatureOfInterest;
	
	//Data properties IRI
	private IRI hasSimpleResult;
	private IRI resultTime;
		
	public SemanticSensor(){
	}
	
	public void initialize () throws RDFParseException, RepositoryException, IOException{
		
		mem = new MemoryStore();
		mem.setPersist(false);
		//mem.initialize();
		rep = new SailRepository(mem);
		rep.initialize();
		f = rep.getValueFactory();
		RDFParser rdfParser = Rio.createParser(RDFFormat.RDFXML);
		model = new LinkedHashModel();
		rdfParser.setRDFHandler(new StatementCollector(model));
		format = Rio.getParserFormatForFileName(file.toString()).orElse(RDFFormat.RDFXML);
		
		//IRI classes		
		Property = f.createIRI(ssnNamespace+"Property");
		FeatureOfInterest = f.createIRI(sosaNamespace+"FeatureOfInterest");
		ApartmentID = f.createIRI(ssnNamespace+"ApartmentID");
		Result = f.createIRI(sosaNamespace+"Result");
		
		//IRI object properties I have to add the classification
		observes = f.createIRI(sosaNamespace + "observes");
		hasProperty = f.createIRI(sosaNamespace + "hasProperty");
		hasDeployment = f.createIRI(ssnNamespace + "hasDeployment");
		madeBySensor = f.createIRI(sosaNamespace+ "madeBySensor");
		observedProperty = f.createIRI(sosaNamespace+ "observedProperty");
		hasResult = f.createIRI(sosaNamespace+"hasResult");
		hasFeatureOfInterest = f.createIRI(sosaNamespace+"hasFeatureOfInterest");
		
		//IRI data properties
		hasSimpleResult = f.createIRI(sosaNamespace+"hasSimpleResult");
		resultTime = f.createIRI(sosaNamespace+"resultTime");
						
		conn = rep.getConnection();
		conn.add(file, ssnNamespace, format);
		
		//conn.add(PropertyInd, RDF.TYPE, Property);
		//conn.add(foiInd, RDF.TYPE, FeatureOfInterest);
		//conn.add(foiInd, hasProperty, PropertyInd);
				
		System.out.println("Ontology initialized");
	}

	//Observation type is the topic sent (DiseaseObservation or EpidemicObservation)
	public Model addDiseaseObservation(Integer apartmentID, String Property, Long timeStamp, String value) throws FileNotFoundException{
		
		sensorOutputModel = null;
		
		Sensor = f.createIRI(ssnNamespace+Property+"CognitiveSensor");
		Observation = f.createIRI(sosaNamespace+Property+"Observation");
		
		IRI sensorIRI = f.createIRI(Sensor.toString()+apartmentID);
		IRI PropertyInd = f.createIRI(ssnNamespace+Property);
		IRI foiInd = f.createIRI(ssnNamespace+apartmentID);
		 			
		IRI observationInd = f.createIRI(ssnNamespace+Property+"Observation"+obsNum++);
		IRI resultIRI = f.createIRI(ssnNamespace+Property);
		
		Literal val = f.createLiteral(value);
		//Literal datetime = f.createLiteral(new Timestamp(date.getTime()));
		Literal datetime = f.createLiteral(timeStamp);
		//he gets msg from grovePi supposons 
		 
		 try  {
			 RepositoryConnection conn = rep.getConnection();
			 try {
				// conn.add(file,  baseURI, format);				 
				// conn.add(sensorIRI, RDF.TYPE, Sensor);
				// conn.add(observationInd, RDF.TYPE, Observation);
				// conn.add(observationInd, observedProperty, PropertyInd);
				// conn.add(observationInd, madeBySensor, Sensor,sensorIRI);
				 conn.add(observationInd, hasFeatureOfInterest, foiInd);
				// conn.add(observationInd, hasResult, resultIRI);
				 conn.add(observationInd, hasSimpleResult, val);
				 conn.add(observationInd, resultTime, datetime);
				// conn.commit();
				 RepositoryResult<Statement> statements = conn.getStatements(null, null, null);
				 model = QueryResults.asModel(statements);
				 
				 RepositoryResult<Statement> sensorOutputStatements = conn.getStatements(observationInd, null, null);			 
				 sensorOutputModel = QueryResults.asModel(sensorOutputStatements);
			 
			 }finally {
			      conn.close();			      
			 }
			// Rio.write(model, System.out, RDFFormat.RDFXML);
			// saveModel(model);
		 }catch (RDF4JException e) {
			   // handle exception
			 System.out.println(e);
		 }catch (Exception e){
			 System.out.println(e);
		 }	
		 return sensorOutputModel;
	}
	
	public Statement parsetoNtriples (String nTriples)throws IOException, RDFParseException, RDFHandlerException {		
		 RDFParser parser=Rio.createParser(RDFFormat.NTRIPLES,f);
		 parser.setPreserveBNodeIDs(true);
		 StatementCollector collector=new StatementCollector();
		 parser.setRDFHandler(collector);
		 parser.parse(new StringReader(nTriples),ssnNamespace);
		 Statement st=collector.getStatements().iterator().next();
		 Rio.write(st, System.out, RDFFormat.RDFXML);
		 return st;
	}
	
	public Model getSensorOutput(){
		//System.out.println(sensorOutputModel);
		return sensorOutputModel;		
	}
	
	public void saveModel(Model model) throws FileNotFoundException{
		OutputStream output = new FileOutputStream("CoSSN_epidemic.owl");
		RDFWriter rdfWriter = Rio.createWriter(format,output);
		try {
			 rdfWriter.startRDF();
			  for (Statement st: model) {
				  rdfWriter.handleStatement(st);
			  }
			  rdfWriter.endRDF();
			}
			catch (RDFHandlerException e) {
			 // oh no, do something!
			}		
	}
	public Timestamp getDatetime(){
		java.util.Date date = new java.util.Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		 
		// Set time fields to zero
		cal.set(Calendar.MILLISECOND, 0);	 
		// Put it back in the Date object
		date = cal.getTime();		
		Timestamp timestamp = new Timestamp(date.getTime());
		return(timestamp);
		
	}

	/*
	public static void main(String[] args) throws RDFParseException, RepositoryException, IOException  {
		
		SemanticSensor adlsim = new SemanticSensor();
		adlsim.initialize();

		System.out.println("100 sensors added successfully");
//		adlsim.addADLSimObservation(24, "ActivityLevel", (long) 212467464 , 2);

			
	}*/	
}
