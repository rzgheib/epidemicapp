package com.semom.adlsim.Epidemic;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.semom.MqttPublisherSubscriber;
import ObservationRDFStreamer.DiseaseStreamer;
import eu.larkc.csparql.core.engine.ConsoleFormatter;
import eu.larkc.csparql.core.engine.CsparqlEngineImpl;
import eu.larkc.csparql.core.engine.CsparqlQueryResultProxy;

public class EpidemicApp extends MqttPublisherSubscriber{
	
	private CsparqlEngineImpl engine;
	private DiseaseStreamer s;		
	private static String[] topics = {"Disease"};
	
	public EpidemicApp() throws RDFParseException, RepositoryException, IOException, MqttException{
		super("EpidemicClient");
		PropertyConfigurator.configure("log4j_configuration/csparql_readyToGoPack_log4j.properties");
		try {
			engine = new CsparqlEngineImpl();
			engine.initialize();							
			s = new DiseaseStreamer("http://streamreasoning.org/streams/diseases", 
					"http://www.streamreasoning.org/ontologies/epidemic-onto#", 1000L);
			//outputStream = new RDFStreamFormatter("http://streamreasoning.org/streams/epidemics");
			
			engine.registerStream(s);
			//engine.registerStream(outputStream);			
			CsparqlQueryResultProxy c = engine.registerQuery(DefineQuery("gastroenteritis"), false);
			//c.addObserver((RDFStreamFormatter) outputStream);
			c.addObserver(new ConsoleFormatter());			
			super.runClient();					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public String DefineQuery(String type) {
		
		String queryBody = null;
		/*if (type.equals("symptomIndicator"))
			queryBody = "REGISTER QUERY IsthereGastroenteritis AS "
				+ "PREFIX : <http://www.streamreasoning.org/ontologies/epidemic-onto#> "
				+ "PREFIX ssn: <http://www.w3.org/ns/ssn/> "
				+ "PREFIX sosa: <http://www.w3.org/ns/sosa/> "
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> "
				
				+ "SELECT DISTINCT ?apart "
				
				+ "FROM STREAM <http://streamreasoning.org/streams/activities> [RANGE 1d STEP 1s] "
				+ "WHERE { "
				+ "?epidemicObservation sosa:hasFeatureOfInterest ?apart . "
				+ "?epidemicObservation sosa:resultTime ?resultTime . "
					+"{ SELECT ?epidemicObservation1 "
					+"WHERE { "
					+ "?epidemicObservation1 sosa:hasFeatureOfInterest ?apart . "
					+ "?epidemicObservation1 sosa:observedProperty ssn:NumberOfToileting ."
					+ "?epidemicObservation1 sosa:hasSimpleResult ?NbToiletResult . "
					+ "FILTER(?NbToiletResult > \"3\"^^xsd:integer) "
					+ "	}}"
					+"{ SELECT ?epidemicObservation2 "
					+"WHERE { "
					+ "?epidemicObservation2 sosa:hasFeatureOfInterest ?apart . "
					+ "?epidemicObservation2 sosa:observedProperty ssn:DurationOfToileting ."
					+ "?epidemicObservation2 sosa:hasSimpleResult ?DrToiletResult . "
					+ "FILTER(?DrToiletResult > \"3\"^^xsd:integer) "
					+ "	}}"
					+"{ SELECT ?epidemicObservation3 "
					+"WHERE { "
					+ "?epidemicObservation3 sosa:hasFeatureOfInterest ?apart . "
					+ "?epidemicObservation3 sosa:observedProperty ssn:NumberOfMeals ."
					+ "?epidemicObservation3 sosa:hasSimpleResult ?NbMealsResult . "
					+ "FILTER(?NbMealsResult < \"3\"^^xsd:integer) "
					+ "	}}"
					+"{ SELECT ?epidemicObservation4 "
					+"WHERE { "
					+ "?epidemicObservation4 sosa:hasFeatureOfInterest ?apart . "
					+ "?epidemicObservation4 sosa:observedProperty ssn:ActivityLevel ."
					+ "?epidemicObservation4 sosa:hasSimpleResult ?ActLevelValue . "
					+ "FILTER(?ActLevelValue < \"3\"^^xsd:integer) "
					+ "	}}"
			+ "}";*/
			
			/*queryBody = "REGISTER QUERY HowManyApartmentEpidemic AS "
						+ "PREFIX : <http://www.streamreasoning.org/ontologies/epidemic-onto#> "
						+ "PREFIX ssn: <http://www.w3.org/ns/ssn/> "
						+ "PREFIX sosa: <http://www.w3.org/ns/sosa/> "
						+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> "
						+ "SELECT ?day (count(DISTINCT ?apart) as ?countapart) (count(DISTINCT ?disease) as ?countD)  "
						+ "FROM STREAM <http://streamreasoning.org/streams/diseases> [RANGE 1d STEP 1s] "
						+ "WHERE { "
						+ "{"
						+ "?diseaseObservation sosa:hasFeatureOfInterest ?apart . "
						+ "?diseaseObservation sosa:resultTime ?day . "
						+ "?diseaseObservation sosa:hasSimpleResult ?disease . "
						+ "FILTER (?disease = \""+type+"\"^^xsd:string) "	
						//+ "FILTER (COUNT(?apart)>50)"
						+ "}"
						+ "UNION"
						+ "{"
						+ "?day ?apart ?disease " 
						+ "}"
						+ "}"
						+ "GROUP BY ?day ";
						//+ "HAVING (?countapart>50)";*/
					
			/*queryBody = "REGISTER QUERY HowManyApartmentEpidemic AS "
					+ "PREFIX : <http://www.streamreasoning.org/ontologies/epidemic-onto#> "
					+ "PREFIX ssn: <http://www.w3.org/ns/ssn/> "
					+ "PREFIX sosa: <http://www.w3.org/ns/sosa/> "
					+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> "
					+ "SELECT ?resultTime (count(DISTINCT ?apart) as ?countapart) "
					+ "FROM STREAM <http://streamreasoning.org/streams/diseases> [RANGE 1s STEP 1s] "
					+ "WHERE { "
					+ "?diseaseObservation sosa:hasFeatureOfInterest ?apart . "
					+ "?diseaseObservation sosa:resultTime ?resultTime . "
					+ "?diseaseObservation sosa:hasSimpleResult ?disease . "
					+ "FILTER (?disease = \""+type+"\"^^xsd:string) "		
					+ "}"
					+ "GROUP BY ?resultTime "
					+ "HAVING ( ?countapart > 20 )";*/
			
			queryBody = "REGISTER QUERY HowManyApartmentEpidemic AS "
					+ "PREFIX : <http://www.streamreasoning.org/ontologies/epidemic-onto#> "
					+ "PREFIX ssn: <http://www.w3.org/ns/ssn/> "
					+ "PREFIX sosa: <http://www.w3.org/ns/sosa/> "
					+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> "
					+ "SELECT DISTINCT ?resultTime (count(DISTINCT ?apart) as ?countapart)  "
					+ "FROM STREAM <http://streamreasoning.org/streams/diseases> [RANGE 1d STEP 1s] "
					+ "WHERE { "
					+ "?diseaseObservation sosa:hasFeatureOfInterest ?apart . "
					+ "?diseaseObservation sosa:resultTime ?resultTime . "
					+ "?diseaseObservation sosa:hasSimpleResult ?disease . "
					+ "FILTER (?disease = \""+type+"\"^^xsd:string) "		
					+ "}"
					+ "GROUP BY ?resultTime "
					+ "HAVING ( ?countapart > 20 )";

		
			return queryBody;
	}
	
	public void messageArrived(String topicSI, MqttMessage message) throws Exception {

		String[] words = message.toString().split("\\)");
				
		int diseaseIndexValue1 = words[1].lastIndexOf(",");
		int diseaseIndexValue2 = words[1].lastIndexOf("\"");
		int apartIndexValue = words[0].lastIndexOf("/");
		int timeIndexValue = words[2].lastIndexOf(",");
		int timeIndexValue2 = words[2].lastIndexOf("\"");
				
		int apart  = Integer.parseInt(words[0].substring(apartIndexValue+1,words[0].length()));
		String disease = words[1].substring(diseaseIndexValue1+3,diseaseIndexValue2 );		
		Long resultTime = Long.parseLong(words[2].substring(timeIndexValue+3,timeIndexValue2 ));
		
		Date date = new Date(resultTime);
	    Format days = new SimpleDateFormat("YYYY MM dd");

		/*System.out.println("Recieved Message :: -----------------------------");
	    System.out.println("| Topic:" + topicSI);
	    System.out.println("| Message: " + new String(message.getPayload()));
	    System.out.println("End ---------------------------------------");*/
	        
		s.addToStream(disease,apart,""+days.format(date) ,resultTime );		    	   
	  }
    
    public static void main(String[] args) throws RDFParseException, RepositoryException, IOException, MqttException, InterruptedException{
    	EpidemicApp App = new EpidemicApp();
    	App.subscribeTO(topics);   	
    }
}